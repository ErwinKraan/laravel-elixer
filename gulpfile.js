(function(){
    'use strict';

    /**************************************
     *          Laravel-Elixer            *
     **************************************/
    var elixir = require('laravel-elixir');

    /**************************************
     *              Config                *
     **************************************/
    elixir.config.sourcemaps = true;
    elixir.config.assetsDir  = 'resources/assets/';
    elixir.config.cssOutput  = 'public/css';
    elixir.config.jsOutput   = 'public/js';
    elixir.config.bowerDir   = 'vendor/bower_components';

    /**************************************
     *          Assets-Functions          *
     **************************************/
    elixir(function(mix){
        mix.sass('main.scss');
        mix.coffee('index.coffee');
    });
}());