# Laravel-Elixer #

Contains the "Assets-Pipeline" for a new project

### What is this repository for? ###

* Asset-Pipeline for a new set-up of a project
* Version: 0.0.1

### How do I get set up? ###

* Clone this project
* Copy the project to the project where you want it to use
* Inside the gulp you can override the folders where files are located and need to be stored
* Inside the .bowerrc you can override the bower installation folder
* Inside the project run npm install
* Add your vendor items like jquery with bower install package-name --save
* Add your vendor items to the bowerBundle inside the gulp file
* Sometimes you want to have the .css files instead of the .min.css this can be override in the bower.json like in the example
* The gulp file contains examples how to use this project
* Use gulp command to vendor build and compile everything one time.
* Use gulp watch --production to watch and compile everything ( bowerBundle ignores a watch so it will not be builded )

### Contribution guidelines ###

* Create a pull request when you want upgrades taken into the repository
* Default Javascript guidelines

### Who do I talk to? ###

* Erwin Kraan